#include <stdio.h>
#include <pwd.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

char film[100][100], thriller[100][100], horror[100][100], comedy[100][100], action[100][100], romance[100][100], school[100][100], fantasy[100][100];
int status;

void makeFolder(char path[]){
    if (fork() == 0){
        execlp("mkdir", "mkdir", "-p", path, NULL);
    }
    while ((wait(&status)) > 0);
}

void extractFile(){
    pid_t child_id;

    child_id = fork();
    if (child_id == 0) {

        char *argv[] = {"unzip", "-q", "/home/aga/drakor.zip", "*.png", "-d", "/home/aga/shift2/drakor", NULL};
        execv("/bin/unzip", argv);
    }
    while ((wait(&status)) > 0);
}

void moveFilm(char path[], int index, char *type){
    strcat(path, film[index]);
    char dest[100];
    if (strcmp(type, "thriller") == 0){
        strcpy(dest, "/home/aga/shift2/drakor/thriller");
    }else if (strcmp(type, "horror") == 0){
        strcpy(dest, "/home/aga/shift2/drakor/horror");
    }else if (strcmp(type, "action") == 0){
        strcpy(dest, "/home/aga/shift2/drakor/action");
    }else if (strcmp(type, "comedy") == 0){
        strcpy(dest, "/home/aga/shift2/drakor/comedy");
    }else if (strcmp(type, "romance") == 0){
        strcpy(dest, "/home/aga/shift2/drakor/romance");
    }else if (strcmp(type, "school") == 0){
        strcpy(dest, "/home/aga/shift2/drakor/school");
    }else if (strcmp(type, "fantasy") == 0){
        strcpy(dest, "/home/aga/shift2/drakor/fantasy");
    }else if (fork() == 0){
        execlp("mv", "mv", path, dest, NULL);
    }
    while ((wait(&status)) > 0);
}

void checkFile(char path[]){
    DIR *dp;
    struct dirent *dir;
    int count = 0, countThriller = 0, countHorror = 0, countAction = 0, countRomance = 0, countSchool = 0, countFantasy = 0, countComedy = 0;

    dp = opendir(path);

    if(dp){
        while((dir = readdir(dp)) != NULL){
            if ((strstr(dir->d_name, "..") == 0) && (strcmp(dir->d_name, "") != 0) && (strcmp(dir->d_name, ".") != 0) && (strstr(dir->d_name, "txt") == 0)){
                strcpy(film[count], dir->d_name);
                count++;
            }
            closedir(dp);
        }
    }

    for (int i = 0; i < count; i++){
        char copyPath[100];
        strcpy(copyPath, path);

        if (strstr(film[i], "thriller")){
            strcpy(thriller[countThriller], film[i]);
            moveFilm(copyPath, i, "thriller");
            countThriller++;
        }
        if (strstr(film[i], "horror")){
            strcpy(horror[countHorror], film[i]);
            moveFilm(copyPath, i, "horror");
            countHorror++;
        }
        if (strstr(film[i], "action")){
            strcpy(action[countAction], film[i]);
            moveFilm(copyPath, i, "action");
            countAction++;
        }
        if (strstr(film[i], "comedy")){
            strcpy(comedy[countComedy], film[i]);
            moveFilm(copyPath, i, "comedy");
            countComedy++;
        }
        if (strstr(film[i], "romance")){
            strcpy(romance[countRomance], film[i]);
            moveFilm(copyPath, i, "romance");
            countRomance++;
        }
        if (strstr(film[i], "school")){
            strcpy(school[countSchool], film[i]);
            moveFilm(copyPath, i, "school");
            countSchool++;
        }
        if (strstr(film[i], "fantasy")){
            strcpy(fantasy[countFantasy], film[i]);
            moveFilm(copyPath, i, "fantasy");
            countFantasy++;
        }
    }
}



int main(){
    char pathShift[] = "/home/aga/shift2";
    char pathDrakor[] = "/home/aga/shift2/drakor";
    char pathThriller[] = "/home/aga/shift2/drakor/thriller";
    char pathHorror[] = "/home/aga/shift2/drakor/horror";
    char pathComedy[] = "/home/aga/shift2/drakor/comedy";
    char pathAction[] = "/home/aga/shift2/drakor/action";
    char pathRomance[] = "/home/aga/shift2/drakor/romance";
    char pathSchool[] = "/home/aga/shift2/drakor/school";
    char pathFantasy[] = "/home/aga/shift2/drakor/fantasy";

    makeFolder(pathShift);
    makeFolder(pathDrakor);
    makeFolder(pathThriller);
    makeFolder(pathHorror);
    makeFolder(pathComedy);
    makeFolder(pathAction);
    makeFolder(pathRomance);
    makeFolder(pathSchool);
    makeFolder(pathFantasy);

    extractFile();
    checkFile(pathDrakor);
    
    return 0;
}
